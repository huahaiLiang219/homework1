package tdlm.controller;

import java.io.File;
import java.io.IOException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

import javax.imageio.ImageIO;
import properties_manager.PropertiesManager;
import tdlm.data.DataManager;
import tdlm.gui.Workspace;
import saf.AppTemplate;
import saf.components.AppDataComponent;
import saf.ui.AppMessageDialogSingleton;
import saf.ui.AppYesNoCancelDialogSingleton;
import tdlm.PropertyType;
import tdlm.data.ToDoItem;

/**
 * This class responds to interactions with todo list editing controls.
 * 
 * @author McKillaGorilla && huahai liang
 * @version 1.0
 */
public class ToDoListController {
    AppTemplate app;
    
    public ToDoListController(AppTemplate initApp) {
	app = initApp;
    }
    
    public void processAddItem() {	
	// ENABLE/DISABLE THE PROPER BUTTONS
	Workspace workspace = (Workspace)app.getWorkspaceComponent();
        
        AddItemForm aif= new AddItemForm();
    
        aif.showTable();
        
         ToDoItem td =  new ToDoItem();
         if(!aif.getCategory().equals("")){
             td.setCategory(aif.getCategory());
         }
         if(!aif.getDescription().equals("")){
             td.setDescription(aif.getDescription());
         }
         if(aif.getStartDate()!=null){
             td.setStartDate(aif.getStartDate());
         }
         if(aif.getEndDate()!=null){
             td.setEndDate(aif.getEndDate());
         }
         td.setCompleted(aif.getCompleted());
        
        if(aif.getSelection().equals(aif.props.getProperty(PropertyType.YES))){
//            ToDoItem tdi =  new ToDoItem(aif.getCategory(),aif.getDescription(),aif.getStartDate(),aif.getEndDate(),aif.getCompleted());
            ((DataManager)app.getDataComponent()).addItem(td);
            app.getGUI().markFileAsNotSaved();
                app.getGUI().updateToolbarControls(false);
            
            //foolproof  design
            Pane pane =(Pane)((VBox)(workspace.getWorkspace().getChildren().get(2))).getChildren().get(1);
            pane.getChildren().get(1).setDisable(false);
            
            if(((DataManager)app.getDataComponent()).getItems().size()>1){
                pane.getChildren().get(2).setDisable(false);
                pane.getChildren().get(3).setDisable(false);
            }
            
        }
    }
    
    /**
     *
     * @throws Exception
     */
    public void processRemoveItem() {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        
        TableView<ToDoItem> table = ((TableView)((VBox)(
                workspace.getWorkspace().getChildren().get(2))).getChildren().get(2));
        ToDoItem itemSelected = (ToDoItem) table.getSelectionModel().getSelectedItem();
        ((DataManager)app.getDataComponent()).getItems().remove(itemSelected);
        
        
        //foolproof  design
        Pane pane =(Pane)((VBox)(workspace.getWorkspace().getChildren().get(2))).getChildren().get(1);
            pane.getChildren().get(1).setDisable(false);
            
            if(((DataManager)app.getDataComponent()).getItems().size()>1){
                pane.getChildren().get(2).setDisable(false);
                pane.getChildren().get(3).setDisable(false);
            }
            if(((DataManager)app.getDataComponent()).getItems().size()==1){
                pane.getChildren().get(2).setDisable(true);
                pane.getChildren().get(3).setDisable(true);
            }
        if(((DataManager)app.getDataComponent()).getItems().isEmpty()){
            pane.getChildren().get(1).setDisable(true);
        }
        app.getGUI().markFileAsNotSaved();
                app.getGUI().updateToolbarControls(false);
    }
    
    public void processMoveUpItem() {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        
        TableView<ToDoItem> table = ((TableView)((VBox)(
                workspace.getWorkspace().getChildren().get(2))).getChildren().get(2));
        int selectedIndex = table.getFocusModel().getFocusedIndex();
        
         Pane pane =(Pane)((VBox)(workspace.getWorkspace().getChildren().get(2))).getChildren().get(1);
        
        if (selectedIndex > 0){ 
            ToDoItem preItem = ((DataManager)app.getDataComponent()).getItems().get(selectedIndex-1);
            ((DataManager)app.getDataComponent()).getItems().remove(preItem);
            ((DataManager)app.getDataComponent()).getItems().add(selectedIndex, preItem); 
            pane.getChildren().get(3).setDisable(false);
        }
        
        //foolproof  design
        else if(selectedIndex == 0){
           
            if(((DataManager)app.getDataComponent()).getItems().size()>1){
                pane.getChildren().get(2).setDisable(true);
                pane.getChildren().get(3).setDisable(false);
            }
        }
        
        app.getGUI().markFileAsNotSaved();
                app.getGUI().updateToolbarControls(false);
        
    }
    
    public void processMoveDownItem() {
         Workspace workspace = (Workspace)app.getWorkspaceComponent();
        TableView<ToDoItem> table = ((TableView)((VBox)(
                workspace.getWorkspace().getChildren().get(2))).getChildren().get(2));
            Pane pane =(Pane)((VBox)(workspace.getWorkspace().getChildren().get(2))).getChildren().get(1);
            
        int selectedIndex = table.getFocusModel().getFocusedIndex();
        if (selectedIndex < ((DataManager)app.getDataComponent()).getItems().size()-1){ 
            ToDoItem afterItem = ((DataManager)app.getDataComponent()).getItems().get(selectedIndex+1);
            ((DataManager)app.getDataComponent()).getItems().remove(afterItem);
            ((DataManager)app.getDataComponent()).getItems().add(selectedIndex, afterItem); 
            pane.getChildren().get(2).setDisable(false);
        }
         

        //foolproof  design
           
        else if(selectedIndex == ((DataManager)app.getDataComponent()).getItems().size()-1){
            if(((DataManager)app.getDataComponent()).getItems().size()>1){
                pane.getChildren().get(3).setDisable(true);
                 pane.getChildren().get(2).setDisable(false);
            }
        }
        app.getGUI().markFileAsNotSaved();
                app.getGUI().updateToolbarControls(false);
    }
    
    public void processEditItem() {
        // ENABLE/DISABLE THE PROPER BUTTONS
	Workspace workspace = (Workspace)app.getWorkspaceComponent();
        
        TableView<ToDoItem> table = ((TableView)((VBox)(
                workspace.getWorkspace().getChildren().get(2))).getChildren().get(2));
            ToDoItem todo=    table.getSelectionModel().getSelectedItem();
        
        AddItemForm aif= new AddItemForm(  
                todo.getCategory(),
                todo.getDescription(),
                todo.getStartDate(),
                todo.getEndDate(),
                todo.getCompleted()
        );
    
        aif.showTable();
        if((aif.props.getProperty(PropertyType.YES)).equals(aif.getSelection())){
            todo.setCategory(aif.getCategory());
            todo.setDescription(aif.getDescription());
            todo.setStartDate(aif.getStartDate());
            todo.setEndDate(aif.getEndDate());
            todo.setCompleted(aif.getCompleted());
            app.getGUI().markFileAsNotSaved();
                app.getGUI().updateToolbarControls(false);
        }
           // table.getSelectionModel().clearSelection();
    }

    public void processDisableButton() {
        Workspace workspace = (Workspace)app.getWorkspaceComponent();
        TableView<ToDoItem> table = ((TableView)((VBox)(
                workspace.getWorkspace().getChildren().get(2))).getChildren().get(2));
        int selectedIndex = table.getFocusModel().getFocusedIndex();
        
        Pane pane =(Pane)((VBox)(workspace.getWorkspace().getChildren().get(2))).getChildren().get(1);
        if(selectedIndex>=0){pane.getChildren().get(1).setDisable(false);}
        
        
        if(((DataManager)app.getDataComponent()).getItems().size()>1){
        
          if(selectedIndex == 0){
                pane.getChildren().get(2).setDisable(true);
                pane.getChildren().get(3).setDisable(false);
                
        }
          else if(selectedIndex == ((DataManager)app.getDataComponent()).getItems().size()-1){
                pane.getChildren().get(2).setDisable(false);
                pane.getChildren().get(3).setDisable(true);
                
        }else{
                pane.getChildren().get(2).setDisable(false);
                pane.getChildren().get(3).setDisable(false);
          }
        }else {
                pane.getChildren().get(2).setDisable(true);
                pane.getChildren().get(3).setDisable(true);
        
        }
    }
}
